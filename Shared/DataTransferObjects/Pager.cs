﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Shared.DataTransferObjects
{
    public class Pager<T> // Generic class 
    {
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }
        public int PageCount { get; set; }
        public List<T> Items { get; set; }
    }
}
