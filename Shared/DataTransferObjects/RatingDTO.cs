﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieReviewSystem.Shared.DataTransferObjects
{
    public class RatingDTO
    {
        public int Rating { get; set; }
        public int MovieShowId { get; set; }
    }
}
