﻿using MovieReviewSystem.Core.Domain.Enums;
using System;
using System.Collections.Generic;

namespace MovieReviewSystem.Shared.DataTransferObjects
{
    public class MovieDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string CoverPhotoUrl { get; set; }
        public Category Category { get; set; }
        public float Rating { get; set; }
        public List<string> Actors { get; set; }
    }
}
