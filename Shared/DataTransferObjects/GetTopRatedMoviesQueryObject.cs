﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Shared.DataTransferObjects
{
    public class GetTopRatedMoviesQueryObject
    {
        public int? Page { get; set; }
        public string? Search { get; set; }
    }
}
