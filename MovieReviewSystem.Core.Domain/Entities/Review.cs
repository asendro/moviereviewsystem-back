﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Core.Domain.Entities
{
    public class Review
    {
        public int Id { get; set; }
        public int Rating { get; set; }
        public MovieShow MovieShow { get; set; }
        public int MovieShowId { get; set; }
    }
}
