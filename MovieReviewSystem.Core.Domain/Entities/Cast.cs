﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Core.Domain.Entities
{
    public class Cast
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
