﻿using MovieReviewSystem.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Core.Domain.Entities
{
    public class MovieShow
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string CoverPhotoUrl { get; set; }
        public List<CastMovie> CastMovies { get; set; }
        public List<Review> Reviews { get; set; }
        public Category Category { get; set; }

    }
}
