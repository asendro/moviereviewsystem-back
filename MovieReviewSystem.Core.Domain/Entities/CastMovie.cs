﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Core.Domain.Entities
{
    public class CastMovie
    {
        public int Id { get; set; }
        public MovieShow MovieShow { get; set; }
        public int MovieShowId { get; set; }
        public Cast Cast { get; set; }
        public int CastId { get; set; }
    }
}
