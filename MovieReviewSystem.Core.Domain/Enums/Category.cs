﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieReviewSystem.Core.Domain.Enums
{
    public enum Category
    {
        Movie = 1,
        Show = 2,
    }
}
