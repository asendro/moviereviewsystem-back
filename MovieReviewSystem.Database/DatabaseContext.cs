﻿using Microsoft.EntityFrameworkCore;
using MovieReviewSystem.Core.Domain.Entities;

namespace MovieReviewSystem.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options):base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }


        public DbSet<MovieShow> MovieShows { get; set; }
        public DbSet<Cast> Casts { get; set; }
        public DbSet<CastMovie> CastMovies { get; set; }
        public DbSet<Review> Reviews { get; set; }

    }
}
