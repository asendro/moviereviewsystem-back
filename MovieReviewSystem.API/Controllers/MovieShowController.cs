﻿using Microsoft.AspNetCore.Mvc;
using MovieReviewSystem.Core.Services.Interfaces;
using MovieReviewSystem.Shared.DataTransferObjects;
using System.Threading.Tasks;

namespace MovieReviewSystem.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieShowController : ControllerBase
    {
        private readonly IMovieShowService _movieService;

        public MovieShowController(IMovieShowService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _movieService.GetAll();
            return Ok(result);
        }

        [HttpGet("top-rated-movies")]
        public async Task<ActionResult> GetTopRatedMovies([FromQuery] GetTopRatedMoviesQueryObject request)
        {
            var result = await _movieService.GetTopRatedMovies(request.Page ?? 1);
            return Ok(result);
        }

        [HttpGet("top-rated-shows")]
        public async Task<ActionResult> GetTopRatedShows([FromQuery] GetTopRatedMoviesQueryObject request)
        {
            var result = await _movieService.GetTopRatedShows(request.Page ?? 1);
            return Ok(result);
        }

        [HttpPost("add-rating")]
        public async Task<ActionResult> AddRatingAsync([FromBody] RatingDTO request)
        {
            await _movieService.AddRating(request.Rating, request.MovieShowId);
            return Ok();
        }
    }
}
