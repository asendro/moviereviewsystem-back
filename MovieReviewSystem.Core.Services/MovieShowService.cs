﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieReviewSystem.Core.Domain.Entities;
using MovieReviewSystem.Core.Services.Interfaces;
using MovieReviewSystem.Database;
using MovieReviewSystem.Shared.Config;
using MovieReviewSystem.Shared.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MovieReviewSystem.Core.Services
{
    public class MovieShowService : IMovieShowService
    {
        private readonly DatabaseContext _context;
        public MovieShowService(DatabaseContext context)
        {
            _context = context;
        }

        public async Task AddRating(int rating, int movieShowId)
        {
            var newReview = new Review
            {
                MovieShowId = movieShowId,
                Rating = rating
            };

            _context.Reviews.Add(newReview);
            await _context.SaveChangesAsync();
        }

        public async Task<Pager<MovieDTO>> GetAll()
        {
            var Items = await _context
                .MovieShows
                .Select(x => new MovieDTO {
                    Id = x.Id,
                    Category = x.Category,
                    CoverPhotoUrl = x.CoverPhotoUrl,
                    Description = x.Description,
                    ReleaseDate = x.ReleaseDate,
                    Title = x.Title,
                    Actors = x.CastMovies.Select(c => c.Cast.FirstName + " " + c.Cast.LastName).ToList(),
                    Rating = x.Reviews.Any() ? (float)x.Reviews.Average(r => r.Rating) : 0
                }).OrderByDescending(x => x.Rating).ToListAsync();

            var pager = new Pager<MovieDTO>() {
                Items = Items,
                TotalItems = Items.Count,
                CurrentPage = 1,
                PageCount = 1
            };
            return pager;
        }

        public async Task<MovieDTO> GetById(int Id)
        {
            return await _context.MovieShows.Where(x => x.Id == Id).Select(x => new MovieDTO {
                Id = x.Id,
                Category = x.Category,
                CoverPhotoUrl = x.CoverPhotoUrl,
                Description = x.Description,
                Rating = x.Reviews.Any() ? (float)x.Reviews.Average(r => r.Rating) : 0,
                ReleaseDate = x.ReleaseDate,
                Title = x.Title
            }).FirstOrDefaultAsync();
        }

        public async Task<Pager<MovieDTO>> GetTopRatedMovies(int Page)
        {
            var items = await _context
                .MovieShows
                .Where(x => x.Category == Domain.Enums.Category.Movie)
                .Select(x => new MovieDTO
                {
                    Id = x.Id,
                    Category = x.Category,
                    CoverPhotoUrl = x.CoverPhotoUrl,
                    Description = x.Description,
                    ReleaseDate = x.ReleaseDate,
                    Title = x.Title,
                    Actors = x.CastMovies.Select(c => c.Cast.FirstName + " " + c.Cast.LastName).ToList(),
                    Rating = x.Reviews.Any() ? (float)x.Reviews.Average(r => r.Rating) : 0
                }).OrderByDescending(x => x.Rating).Skip(Constants.ItemsPerPage * (Page - 1)).Take(Constants.ItemsPerPage).ToListAsync();

            var total = _context.MovieShows.Count(x => x.Category == Domain.Enums.Category.Movie);

            var pager = new Pager<MovieDTO>()
            {
                Items = items,
                TotalItems = total,
                CurrentPage = Page,
                PageCount = (int)Math.Round((double)(total / Constants.ItemsPerPage)) + 1
            };
            return pager;
        }

        public async Task<Pager<MovieDTO>> GetTopRatedShows(int Page)
        {
            var Items = await _context
                .MovieShows
                .Where(x => x.Category == Domain.Enums.Category.Show)
                .Select(x => new MovieDTO
                {
                    Id = x.Id,
                    Category = x.Category,
                    CoverPhotoUrl = x.CoverPhotoUrl,
                    Description = x.Description,
                    ReleaseDate = x.ReleaseDate,
                    Title = x.Title,
                    Actors = x.CastMovies.Select(c => c.Cast.FirstName + " " + c.Cast.LastName).ToList(),
                    Rating = x.Reviews.Any() ? (float)x.Reviews.Average(r => r.Rating) : 0
                }).OrderByDescending(x => x.Rating).Skip(Constants.ItemsPerPage * (Page - 1)).Take(Constants.ItemsPerPage).ToListAsync();

            var total = _context.MovieShows.Count(x => x.Category == Domain.Enums.Category.Show);

            var pager = new Pager<MovieDTO>()
            {
                Items = Items,
                TotalItems = total,
                CurrentPage = Page,
                PageCount = (int)Math.Round((double)(total / Constants.ItemsPerPage)) + 1
            };
            return pager;
        }
    }
}
