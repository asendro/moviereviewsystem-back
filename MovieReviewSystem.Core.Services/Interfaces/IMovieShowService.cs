﻿using Microsoft.AspNetCore.Mvc;
using MovieReviewSystem.Shared.DataTransferObjects;
using System.Threading.Tasks;

namespace MovieReviewSystem.Core.Services.Interfaces
{
    public interface IMovieShowService
    {
        Task<Pager<MovieDTO>> GetAll();
        Task<Pager<MovieDTO>> GetTopRatedMovies(int Page);
        Task<Pager<MovieDTO>> GetTopRatedShows(int Page);
        Task<MovieDTO> GetById(int Id);
        Task AddRating(int rating, int movieShowId);
    }
}
