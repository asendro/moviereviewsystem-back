﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MovieReviewSystem.Core.Services.Interfaces
{
    public interface IRatingService
    {
        Task<ActionResult> RateMovie(int MovieId, int Rate);
    }
}
